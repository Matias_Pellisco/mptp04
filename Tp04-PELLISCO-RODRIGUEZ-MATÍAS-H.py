import os
def menu():
    print('-'*20,'MENÚ PRINCIPAL','-'*20)
    print('a)Registrar un producto.')
    print('b)Mostrar el listado de productos.')
    print('c)Mostrar los productos cuyo stock se encuentre en un intervalo.')
    print('d)Sumar X a stock de todos los productos que tengan un valor menor que Y.')
    print('e)Eliminar todos los productos con stock cero.')
    print('f)Salir')
    print('-'*56)
    while True:
        opc = input('Seleccione una opción valida: ')
        if opc in ['a', 'b', 'c', 'd', 'e','f']:
            return opc

def validarStock():
    while True:
        try:
            n = int(input('Ingrese Stock del producto: \n'))
            while (n<0):
                n = int(input('Error, ingrese Stock valido: \n'))
            return n
        except ValueError:
            print('Error, ingrese Stock valido:')
            continue

def validarNumero(mensaje):
    while True:
        try:
            n = int(input(mensaje))
            return n
        except ValueError:
            print('Error, debe ingresar un número entero.')
            continue

def validarPrecio():
    while True:
        try:
            n = float(input('Ingrese precio del producto: \n'))
            while (n<0):
                n = float(input('Error, ingrese un precio valido: \n'))
            return n
        except ValueError:
            print('Error, ingrese un precio valido:')
            continue

def validarCodigo():
    #Valida que el código sea un número
    d=-1   
    while True:
        try:
            a = int(input('Ingrese el código del producto: \n'))
            while (a<0):
                a = int(input('Error, ingrese un codigo valido: \n'))
            return a           
        except ValueError:
            print('Error, ingrese un codigo valido:')
            continue 

def registrarProducto(agregarProducto):
    print('-'*20,'REGISTRANDO PRODUCTO', '-'*20)
    codigoDeProducto=validarCodigo()
    descripcion=input('Ingrese descripción del producto: \n')
    precio=validarPrecio()
    stock=validarStock()
    agregarProducto[codigoDeProducto]=[descripcion,precio,stock]
    
def mostrarProductos(productos): 
    print('-'*20,'PRODUCTOS REGISTRADOS','-'*20)   
    for codigo, datos in productos.items():
        print('Codigo de producto: ',codigo,' Descripción: ', datos[0],' Precio: ', datos[1],' Stock: ',datos[2])
    input('Presione ENTER para continuar.')

def eliminarStockCero(productos):
    for codigo, datos in productos.items():
        if(datos[2]==0):
            n=codigo
            del productos[n]
        
def intervaloStock(productos):
    desde=validarNumero('Ingrese stock mínimo:\n')
    hasta=validarNumero('Ingrese stock máximo:\n')
    print('-'*20, 'Productos en rango de stock ingresado', '-'*20)  
    for codigo, datos in productos.items():
        if(datos[2]>=desde and datos[2]<=hasta):          
            print('Codigo de producto: ',codigo,' Descripción: ', datos[0],' Precio: ', datos[1],' Stock: ',datos[2])
        else:
            print('No se encontraron productos en el rango solicitado. ')
    input('\nPresione ENTER para continuar.')

def reposicionDeStock(productos):
    print('-'*20,'REPOSICIÓN DE STOCK','-'*20)
    stockMin= validarNumero('Ingrese stock mínimo:\n')
    reposicion=validarNumero('Ingrese la cantidad a reponer:\n')
    i=0
    for codigo,datos in list(productos.items()):
        if(datos[2]<=stockMin):
            i+=1
            n=datos[2]
            productos[codigo]=[datos[0],datos[1],n+reposicion]           
    print('Se actualizó el stock de ',i,' productos. \n') 
    input('Presione ENTER para continuar.')
#Principal

productos={}
opc='?'
while(opc != 'f'):
    os.system('cls')
    opc=menu()
    if(opc=='a'):
        registrarProducto(productos)
    elif(opc=='b'):
        mostrarProductos(productos)
    elif(opc=='c'):
        intervaloStock(productos)
    elif(opc=='d'):
        reposicionDeStock(productos)
    elif(opc=='e'):
        productos=eliminarStockCero(productos)